package at.fhv.jla3941.gutenberg.controller.commands.file;

import at.fhv.jla3941.gutenberg.controller.commands.ReadFileCMD;
import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.controller.commands.CompareStoredFileCMD;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Johannes
 */
public class OpenCMD implements ICommand {

    private GDocument gDocument;
    private File file;

    /**
     *
     * @param document
     */
    public OpenCMD(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Opens a new file.
     */
    @Override
    public void execute() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files (.c, .h, .java, .txt, .md, .sv, .v)", "txt", "c", "java", "md", "sv", "v", "h");
        chooser.setFileFilter(filter);
        boolean areEqual = false;
        int responseOpenDialog = 0;
        chooser.setAcceptAllFileFilterUsed(false);

        // Check if text has changed and file was not stored
        if (!gDocument.getjTextPane().getText().isEmpty() && gDocument.getFile() == null) {
            responseOpenDialog = JOptionPane.showConfirmDialog(gDocument.getjFrame(), "Do you want to save changes to\n" + gDocument.getFileName(), "Confirm",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (responseOpenDialog == 0) {
                SaveCMD save = new SaveCMD(gDocument);
                save.execute();

                chooser.showOpenDialog(gDocument.getjFrame());
                chooser.setVisible(true);
                file = chooser.getSelectedFile();
                ReadFileCMD readFile = new ReadFileCMD(gDocument, file);
                readFile.execute();
            }
            else{
                chooser.showOpenDialog(gDocument.getjFrame());
                chooser.setVisible(true);
                file = chooser.getSelectedFile();
                ReadFileCMD readFile = new ReadFileCMD(gDocument, file);
                readFile.execute();        
            }
            
        } // Check if stored file has changed
        else if (gDocument.getFile() != null) {
            CompareStoredFileCMD compare = new CompareStoredFileCMD(gDocument);
            compare.execute();
            areEqual = compare.areEqual();

            if (!areEqual) {
                responseOpenDialog = JOptionPane.showConfirmDialog(gDocument.getjFrame(), "Do you want to save changes to \n" + gDocument.getFileName(), "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            }

            if (responseOpenDialog == 0) {

                SaveCMD save = new SaveCMD(gDocument);
                save.execute();

                chooser.showOpenDialog(gDocument.getjFrame());
                chooser.setVisible(true);

                file = chooser.getSelectedFile();
                ReadFileCMD readFile = new ReadFileCMD(gDocument, file);
                readFile.execute();

            } else {
                chooser.showOpenDialog(gDocument.getjFrame());
                chooser.setVisible(true);
                file = chooser.getSelectedFile();
                ReadFileCMD readFile = new ReadFileCMD(gDocument, file);
                readFile.execute();
            }
        }

        // Check if ok or cancel was pressed during readFile dialog
        if (gDocument.getjTextPane().getText().isEmpty() && gDocument.getFile() == null) {
            responseOpenDialog = chooser.showOpenDialog(gDocument.getjFrame());

            if (responseOpenDialog == 0) {
                chooser.setVisible(true);
                file = chooser.getSelectedFile();
                ReadFileCMD readFile = new ReadFileCMD(gDocument, file);
                readFile.execute();
            }
        }

    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
