/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.controller.Invoker;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;

/**
 *
 * @author Johannes
 */
public class ReadFileCMD implements ICommand {

    private GDocument gDocument;
    private File file;
    private JTextPane bufferJTextPane;
    private Invoker invoke;
    private GetFileTypeCMD fileType;

    public ReadFileCMD(GDocument document, File fileToOpen) {
        this.gDocument = document;
        this.file = fileToOpen;
        this.bufferJTextPane = new JTextPane();
        this.invoke = new Invoker();
    }

    @Override
    public void execute() {

        if (file != null && file.exists() && file.canRead()) {

            try {
                try (BufferedReader readTextFile = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
                    Object device = null;
                    bufferJTextPane.read(readTextFile, device);
                }
                bufferJTextPane.requestFocus();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
            }
            // copy text in bufferTextPane
            bufferJTextPane.selectAll();
            bufferJTextPane.copy();

            // set the filename
            gDocument.setFileName(file.getPath());

            // set syntax
            fileType = new GetFileTypeCMD(gDocument);
            fileType.execute();
            invoke.execute(new ChangeSyntaxCMD(gDocument, gDocument.getFileType()));           

            if (!bufferJTextPane.getText().isEmpty()) { // this is needed, otherwise the last content in the clipboard will be paste
                // if the bufferJTextPane was empty
                
                // clean the JTextPane            
                gDocument.getjTextPane().setText("");
                // insert text ...
                gDocument.getjTextPane().paste();
                gDocument.getjTextPane().requestFocus();
                gDocument.getjTextPane().setCaretPosition(0);
                gDocument.setFile(file);
                gDocument.getjFrame().setTitle(gDocument.getFileName());
            }

        }
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
