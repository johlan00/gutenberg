/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller;

/**
 *
 * @author Johannes
 */
public class Invoker {

    /**
     * Executes the command given to the function
     *
     * @param command
     */
    public void execute(final ICommand command) {
        command.execute();
    }

    /**
     * Unexecutes the command given to the function
     *
     * @param command
     */
    public void unexecute(final ICommand command) {
        command.unexecute();
    }
}
