/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller;

/**
 *
 * @author Johannes
 */
public interface ICommand {

    /**
     * execute() command interface
     */
    void execute();

    /**
     * unexecute() command interface
     */
    void unexecute();

}
