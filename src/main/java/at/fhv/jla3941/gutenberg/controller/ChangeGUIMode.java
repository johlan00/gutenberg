/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller;

import at.fhv.jla3941.gutenberg.model.GDocument;
import java.awt.Color;

/**
 *
 * @author Johannes
 */
public class ChangeGUIMode {

    private GDocument gDocument;

    /**
     * Sets the needed GDocument
     *
     * @param document
     */
    public void setGDocument(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Changes the color of the JTextPane for needed mode (night or day mode)
     */
    public void changeGUIMode() {

        if (gDocument.getGuiState() == false) {
            gDocument.getjTextPane().setForeground(Color.WHITE);
            gDocument.getjTextPane().setCaretColor(Color.WHITE);
            gDocument.getjTextPane().setBackground(Color.DARK_GRAY);
            gDocument.getjTextPane().updateUI(); // this is needed, without this the foreground colour will be black until the first line space will force the JTextPane to refresh
            gDocument.changeGuiState();

        } else {
            gDocument.getjTextPane().setCaretColor(Color.BLACK);
            gDocument.getjTextPane().setBackground(Color.WHITE);
            gDocument.getjTextPane().setForeground(Color.BLACK);
            gDocument.getjTextPane().updateUI();
            gDocument.changeGuiState();
        }
    }

}
