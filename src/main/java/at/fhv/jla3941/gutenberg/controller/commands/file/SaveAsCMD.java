/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.file;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import at.fhv.jla3941.gutenberg.persistence.saveAs;

/**
 *
 * @author Johannes
 */
public class SaveAsCMD implements ICommand {

    private GDocument gDocument;
    private saveAs save;

    /**
     *
     * @param document
     */
    public SaveAsCMD(GDocument document) {
        this.gDocument = document;
        this.save = new saveAs(gDocument);
    }

    /**
     * Executes saveAs where a Save Dialog will be opened
     */
    @Override
    public void execute() {
        save.SaveAs();
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
