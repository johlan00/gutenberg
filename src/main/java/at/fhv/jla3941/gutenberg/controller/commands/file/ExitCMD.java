/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.file;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.controller.commands.CompareStoredFileCMD;
import at.fhv.jla3941.gutenberg.model.GDocument;
import javax.swing.JOptionPane;

/**
 *
 * @author Johannes
 */
public class ExitCMD implements ICommand {

    private GDocument gDocument;

    /**
     * Constructor for the ExitCMD
     *
     * @param document
     */
    public ExitCMD(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Exits Gutenberg and checks in advance for unsaved changes
     *
     */
    @Override
    public void execute() {
        int response = 0;
        // If file is stored already once
        ///read file in the stored path and compare the text with the text in the stored file
        if (gDocument.getFileName() != null && !"Untitled".equals(gDocument.getFileName())) {

            CompareStoredFileCMD compare = new CompareStoredFileCMD(gDocument);
            compare.execute();

            if (!compare.areEqual()) {
                response = JOptionPane.showConfirmDialog(gDocument.getjFrame(), "Do you want to save changes to \n" + gDocument.getFileName() + "?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            }

            if (response == 0) {
                SaveCMD save = new SaveCMD(gDocument);
                save.execute();
                gDocument.getjFrame().dispose();
            } else {
                gDocument.getjFrame().dispose();
            }

            // If file was not stored and no text editet
        } else if ("Untitled".equals(gDocument.getFileName()) && gDocument.getjTextPane().getText().isEmpty()) {
            gDocument.getjFrame().dispose();

        } else {
            // If file was not saved but text changed 
            if (!gDocument.getjTextPane().getText().isEmpty()) {
                response = JOptionPane.showConfirmDialog(gDocument.getjFrame(), "Do you want to save changes to Untitled?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (response == 0) {
                    SaveAsCMD saveAs = new SaveAsCMD(gDocument);
                    saveAs.execute();
                    gDocument.getjFrame().dispose();
                } else {
                    gDocument.getjFrame().dispose();
                }
            }
        }
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
