/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;

/**
 *
 * @author Johannes
 */
public class FindCMD implements ICommand {

    private GDocument gDocument;
    private String textToFind;

    /**
     * Constructor for the FindCMD.
     *
     * @param document
     * @param findText
     */
    public FindCMD(GDocument document, String findText) {
        this.gDocument = document;
        this.textToFind = findText;
    }

    /**
     * Highlights the text to find
     */
    @Override
    public void execute() {
        //<editor-fold defaultstate="collapsed" desc="        Highlight         ">
        Highlighter.HighlightPainter highlighter = new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);
        unexecute();

        if (!textToFind.isEmpty()) {
            try {
                Highlighter highlight = gDocument.getjTextPane().getHighlighter();
                Document doc = gDocument.getjTextPane().getDocument();
                gDocument.getjTextPane().select(0, 0); // This line is needed, otherwise the system text selection will cause GUI glitches
                String text = doc.getText(0, doc.getLength());

                int position = 0;

                while ((position = text.toUpperCase().indexOf(textToFind.toUpperCase(), position)) >= 0) {
                    highlight.addHighlight(position, position + textToFind.length(), highlighter);
                    position += textToFind.length();
                }

            } catch (BadLocationException e) {
                JOptionPane.showMessageDialog(gDocument.getjFrame(), "Caught Error: " + e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);

            }
        }
    }
    //</editor-fold>

    /**
     * Unhighlights everything
     */
    @Override
    public void unexecute() {
        //<editor-fold defaultstate="collapsed" desc="        Unhilight         ">
        /**
         * Unhighlights every highlighted text
         */
        Highlighter removeHighlighter = gDocument.getjTextPane().getHighlighter();
        Highlighter.Highlight[] remove = removeHighlighter.getHighlights();

        for (int i = 0; i < remove.length; i++) {
            removeHighlighter.removeHighlight(remove[i]);
        }
    }
    //</editor-fold>

    /**
     *
     * @return
     */
    public String getTextToFind() {
        return textToFind;
    }
}
