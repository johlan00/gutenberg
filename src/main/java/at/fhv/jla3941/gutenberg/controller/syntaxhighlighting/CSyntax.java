/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.syntaxhighlighting;

import at.fhv.jla3941.gutenberg.model.GDocument;
import static at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.MultiSyntaxDocument.DEFAULT_KEYWORD;
import java.util.HashMap;
import javax.swing.text.MutableAttributeSet;

/**
 *
 * @author Johannes
 */
public class CSyntax implements ISyntax {

    private GDocument gDocument;
    private HashMap<String, MutableAttributeSet> keywords;
    private MultiSyntaxDocument syntaxDocument;

    public CSyntax(GDocument document) {
        this.keywords = new HashMap<>();

        /* Johannes:
         * C Keywords according to
         * http://en.cppreference.com/w/c/keyword
         */
        this.gDocument = document;
        this.syntaxDocument = new MultiSyntaxDocument(keywords, document);

        syntaxDocument.addKeyword("auto", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("break", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("case", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("char", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("const", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("continue", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("default", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("do", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("double", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("else", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("enum", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("extern", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("float", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("for", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("goto", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("if", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("inline", DEFAULT_KEYWORD); // since C99
        syntaxDocument.addKeyword("int", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("long", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("register", DEFAULT_KEYWORD); // since C99
        syntaxDocument.addKeyword("restrict", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("return", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("signed", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("sizeof", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("static", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("struct", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("switch", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("typedef", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("union", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("unsigned", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("void", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("volatile", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("while", DEFAULT_KEYWORD);

        // keywords begining with an underscore are usually used through macros
        syntaxDocument.addKeyword("_Alignas", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Alingof", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Atomic", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Bool", DEFAULT_KEYWORD);   // since C99
        syntaxDocument.addKeyword("_Complex", DEFAULT_KEYWORD); // since C99
        syntaxDocument.addKeyword("_Generic", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Imaginary", DEFAULT_KEYWORD); // since C99
        syntaxDocument.addKeyword("_Noreturn", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Static_assert", DEFAULT_KEYWORD); // since C11
        syntaxDocument.addKeyword("_Thread_local", DEFAULT_KEYWORD); // since C11
        
        syntaxDocument.setFontSize(gDocument.getFontSize());
        gDocument.getjTextPane().setDocument(syntaxDocument);
    }

}
