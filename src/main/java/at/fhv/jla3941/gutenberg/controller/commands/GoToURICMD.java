/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.view.Gutenberg;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Johannes
 */
public class GoToURICMD implements ICommand {
    
    private String uri;

    public GoToURICMD(String goToUri) {
        this.uri = goToUri;
    }
    
    

    @Override
    public void execute() {
        if(Desktop.isDesktopSupported()){
            try {
                Desktop.getDesktop().browse(new URI(uri));
            } catch (URISyntaxException | IOException ex) {
                Logger.getLogger(Gutenberg.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
