/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.file;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import at.fhv.jla3941.gutenberg.persistence.Save;

/**
 *
 * @author Johannes
 */
public class SaveCMD implements ICommand {

    private Save save;
    private GDocument gDocument;

    /**
     * Constructor for the SaveCMD
     *
     * @param document
     */
    public SaveCMD(GDocument document) {
        this.gDocument = document;
        save = new Save(gDocument);
    }

    /**
     * Executes the SaveCMD. An already stored file will be overwritten. An
     * unsaved file will cause the SaveCMD to execute the SaveAsCMD
     */
    @Override
    public void execute() {
        save.save();
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
