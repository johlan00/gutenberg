package at.fhv.jla3941.gutenberg.persistence;

import at.fhv.jla3941.gutenberg.model.GDocument;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author Johannes
 */
public class Save {

    private GDocument gDocument;

    /**
     *
     * @param document
     */
    public Save(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Checks if the open text file is already stored somewhere. If yes: it
     * overwrites the stored file. If no : it does nothing
     */
    public void save() {

        if (gDocument.getFile() != null && gDocument.getFile().exists() && gDocument.getFile().canWrite()) {

            try {
                try (FileWriter fileWriter = new FileWriter(gDocument.getFile())) {
                    fileWriter.write(gDocument.getjTextPane().getText());
                }

            } catch (IOException e) {
                System.out.println("Caught IOException: " + e.getMessage());
                JOptionPane.showMessageDialog(gDocument.getjFrame(), "IO Exception during Save Dialog" + e.getMessage(), "Error Message", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            saveAs saveAs = new saveAs(gDocument);
            saveAs.SaveAs();
        }
    }

}
