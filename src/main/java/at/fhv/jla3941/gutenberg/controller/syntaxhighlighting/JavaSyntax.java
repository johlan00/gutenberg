/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.syntaxhighlighting;

import at.fhv.jla3941.gutenberg.model.GDocument;
import static at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.MultiSyntaxDocument.DEFAULT_KEYWORD;
import java.util.HashMap;
import javax.swing.text.MutableAttributeSet;

/**
 *
 * @author Johannes
 */
public class JavaSyntax implements ISyntax {

    private GDocument gDocument;
    private HashMap<String, MutableAttributeSet> keywords;
    private MultiSyntaxDocument syntaxDocument;

    public JavaSyntax(GDocument document) {
        this.keywords = new HashMap<>();

        /* Johannes:
         * Java Keywords according to
         * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
         * true, false, and null might seem like keywords, but they are actually literals
         */
        this.gDocument = document;
        this.syntaxDocument = new MultiSyntaxDocument(keywords, document);

        syntaxDocument.addKeyword("abstract", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("assert", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("boolean", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("break", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("byte", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("case", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("catch", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("char", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("class", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("const", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("continue", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("default", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("do", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("double", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("enum", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("extends", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("else", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("false", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("final", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("finally", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("float", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("for", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("goto", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("if", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("implements", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("import", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("instanceof", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("int", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("interface", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("long", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("native", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("new", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("null", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("package", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("private", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("protected", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("public", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("return", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("short", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("static", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("super", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("switch", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("synchronized", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("this", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("throw", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("throws", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("transient", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("try", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("void", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("volatile", DEFAULT_KEYWORD);
        syntaxDocument.addKeyword("while", DEFAULT_KEYWORD);

        syntaxDocument.setFontSize(gDocument.getFontSize());
        gDocument.getjTextPane().setDocument(syntaxDocument);
    }

}
