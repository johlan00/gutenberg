/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.settings;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.awt.Font;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

/**
 *
 * @author Johannes
 */
public class ChangeFontSizeCMD implements ICommand{
    
    private GDocument gDocument;
    private int fontSize;
    private JFormattedTextField jFontSizeField;

    public ChangeFontSizeCMD(GDocument document, JFormattedTextField fontSizeField) {
        this.gDocument = document; 
        this.jFontSizeField = fontSizeField;
        this.fontSize = gDocument.getFontSize();
    }
    
    

    @Override
    public void execute() {
        
        try {
            fontSize = Integer.parseInt((jFontSizeField.getText()));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(gDocument.getjFrame(), "Number Format Exception!\nPlease enter an integer number!", "Error Message", JOptionPane.ERROR_MESSAGE);
        }
        
        if(fontSize < 5){
            fontSize = 5;
        }
        
        if(fontSize > 20){
            fontSize = 20;
        }

        gDocument.setFontSize(fontSize);
        Font font = new Font("DEFAULT", 0, fontSize);
        gDocument.getjTextPane().setFont(font);
    }

    @Override
    public void unexecute() {
        
    }
    
}
