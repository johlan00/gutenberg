/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import java.awt.GraphicsDevice;
import javax.swing.JFrame;

/**
 *
 * @author Johannes
 */
public class CenterCMD implements ICommand {

    private JFrame jFrame;
    private int screenWidth;
    private int screenHeight;
    private GraphicsDevice device ;

    /**
     * Constructor for the CenterCMD when using a JFrame
     *
     * @param frame
     */
    public CenterCMD(JFrame frame) {
        this.jFrame = frame;
        this.device = jFrame.getGraphicsConfiguration().getDevice();
        this.screenWidth = device.getDisplayMode().getWidth();
        this.screenHeight = device.getDisplayMode().getHeight();
    }


    /**
     * Centers the JFrame given in the constructor
     */
    @Override
    public void execute() {

        if (jFrame != null) {
            screenWidth = (screenWidth - jFrame.getWidth()) / 2;
            screenHeight = (screenHeight - jFrame.getHeight()) / 2;
            jFrame.setLocation(screenWidth, screenHeight);

        }
            
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
