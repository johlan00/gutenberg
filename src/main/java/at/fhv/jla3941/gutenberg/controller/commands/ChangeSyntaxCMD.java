/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.CSyntax;
import at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.DefaultSyntax;
import at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.ISyntax;
import at.fhv.jla3941.gutenberg.controller.syntaxhighlighting.JavaSyntax;

/**
 *
 * @author Johannes
 */
public class ChangeSyntaxCMD implements ICommand {

    private GDocument gDocument;
    private ISyntax syntax;
    private String fileEnding;

    /**
     * Constructor for the ChangeSyntaxCMD
     *
     * @param document
     * @param fileType
     */
    public ChangeSyntaxCMD(GDocument document, String fileType) {
        this.gDocument = document;
        this.fileEnding = fileType;
    }

    /**
     * Sets the syntax depending on the file type of the opened file.
     */
    @Override
    public void execute() {
        switch (fileEnding) {
            case "java":
                syntax = new JavaSyntax(gDocument);
                break;

            case "c":
                syntax = new CSyntax(gDocument);
                break;

            case "h":
                syntax = new CSyntax(gDocument);
                break;

            default:
                syntax = new DefaultSyntax();
                break;
        }
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
