/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;

/**
 *
 * @author Johannes
 */
public class GetFileTypeCMD implements ICommand {

    private GDocument gDocument;
    private String fileType;

    /**
     * Constructor for the GetFileTypeCMD
     *
     * @param document
     */
    public GetFileTypeCMD(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Stores the fileType in the GDocument
     */
    @Override
    public void execute() {
        int position = gDocument.getFileName().indexOf(".");
        fileType = gDocument.getFileName().substring(position + 1, gDocument.getFileName().length());
        gDocument.setFileType(fileType);
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
