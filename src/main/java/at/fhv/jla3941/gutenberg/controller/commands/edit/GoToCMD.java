/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.text.Element;

public class GoToCMD implements ICommand {

    private GDocument gDocument;
    private JFormattedTextField jLineNumberField;
    private int line;

    /**
     * Constructor for the GoToCMD
     *
     * @param document
     * @param textField
     */
    public GoToCMD(GDocument document, JFormattedTextField textField) {
        this.gDocument = document;
        this.jLineNumberField = textField;
    }

    /**
     * Jumps to the line number given by the textField variable.
     */
    @Override
    public void execute() {

        try {
            line = Integer.parseInt((jLineNumberField.getText()));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(gDocument.getjFrame(), "Number Format Exception!\nPlease enter an integer number!", "Error Message", JOptionPane.ERROR_MESSAGE);
        }

        Element root = gDocument.getjTextPane().getDocument().getDefaultRootElement();
        line = Math.max(line, 1);
        line = Math.min(line, root.getElementCount());
        int startOfLineOffset = root.getElement(line - 1).getStartOffset();
        gDocument.getjTextPane().setCaretPosition(startOfLineOffset);
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
