/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.settings;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import at.fhv.jla3941.gutenberg.controller.ChangeGUIMode;

/**
 *
 * @author Johannes
 */
public class ChangeGUIModeCMD implements ICommand {

    private ChangeGUIMode changeGUI = new ChangeGUIMode();

    /**
     * Constructor for the ChangeGUIModeCMD
     *
     * @param document
     */
    public ChangeGUIModeCMD(GDocument document) {
        changeGUI.setGDocument(document);
    }

    /**
     * execute the change of the GUIMode
     */
    @Override
    public void execute() {
        changeGUI.changeGUIMode();
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
