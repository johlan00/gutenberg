/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;

/**
 *
 * @author Johannes
 */
public class ReplaceAllCMD implements ICommand {

    private GDocument gDocument;
    private FindCMD findCMD;
    private String text;
    private String newText;

    /**
     * Constructor for the ReplaceAllCMD
     *
     * @param document
     * @param find
     * @param replaceText
     */
    public ReplaceAllCMD(GDocument document, FindCMD find, String replaceText) {
        this.gDocument = document;
        this.findCMD = find;
        this.text = find.getTextToFind();
        this.newText = replaceText;
        findCMD.execute();
    }

    /**
     * Replaces a specific text given from the Find field with a new text given
     * from the replace field.
     */
    @Override
    public void execute() {

        if (!text.isEmpty() && !newText.isEmpty()) {
            gDocument.getjTextPane().setText(gDocument.getjTextPane().getText().replaceAll(text, newText));
            gDocument.getjTextPane().setCaretPosition(0);
        }

    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
