/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;

/**
 *
 * @author Johannes
 */
public class SelectAllCMD implements ICommand {

    private GDocument gDocument;

    /**
     * Constructor for the MarkAllCMD
     *
     * @param document
     */
    public SelectAllCMD(GDocument document) {
        this.gDocument = document;
    }

    /**
     * SelectAll the whole text stored in the GDocument.
     */
    @Override
    public void execute() {
        gDocument.getjTextPane().selectAll();
    }

    /**
     * Undoes any selection.
     */
    @Override
    public void unexecute() {
        gDocument.getjTextPane().select(0, 0);
    }

}
