/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.model;

import java.io.File;
import javax.swing.JFrame;
import javax.swing.JTextPane;

/**
 *
 * @author Johannes
 */
public class GDocument {

    private JFrame jFrame;
    private JTextPane jTextPane;
    private String fileName;
    private String fileType;
    private File file;
    private int fontSize;
    private boolean guiState;

    /**
     *
     * @param textPane
     * @param frame
     */
    public GDocument(JTextPane textPane, JFrame frame) {
        this.jTextPane = textPane;
        this.jFrame = frame;
        this.guiState = false; // Set default state for GUIMode in this case --> day mode
        this.fileName = "Untitled";
        this.jFrame.setTitle(fileName);
        this.fontSize = 12; // Default font size
    }

    /**
     * sets the JTextPanel
     *
     * @param jTextPane
     */
    public void setjTextPane(JTextPane jTextPane) {
        this.jTextPane = jTextPane;
    }

    /**
     * sets the filename in GDocument
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Changes the state of the guiState variable by negating the variable
     */
    public void changeGuiState() {
        this.guiState = !guiState;
    }

    /**
     * sets the file in GDocument
     *
     * @param file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * @return the filename
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @return the JTextPane stored in the GDocument
     */
    public JTextPane getjTextPane() {
        return jTextPane;
    }

    /**
     * @return the file stored in the GDocument
     */
    public File getFile() {
        return file;
    }

    /**
     * @return the actual guiState
     */
    public boolean getGuiState() {
        return guiState;
    }

    /**
     * @return the jFrame
     */
    public JFrame getjFrame() {
        return jFrame;
    }

    /**
     * set the actual guiState in GDocument
     *
     * @param guiState
     */
    public void setGuiState(boolean guiState) {
        this.guiState = guiState;
    }

    /**
     * @return the actual file type
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * set the actual guiState in GDocument
     *
     * @param fileType
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
    
    /**
     * get the actual font size
     * @return 
     */

    public int getFontSize() {
        return fontSize;
    }
    
    /**
     * get the actual font size as a string
     * @return 
     */
    
    public String getFontSizeAsString(){  
        return Integer.toString(fontSize);
    }
    
    /**
     * set the actual guiState in GDocument
     *
     * @param fontSize
     */

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }
    
}
