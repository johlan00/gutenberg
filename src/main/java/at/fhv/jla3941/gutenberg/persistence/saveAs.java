/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.persistence;

import at.fhv.jla3941.gutenberg.controller.Invoker;
import at.fhv.jla3941.gutenberg.controller.commands.GetFileTypeCMD;
import at.fhv.jla3941.gutenberg.controller.commands.ChangeSyntaxCMD;
import at.fhv.jla3941.gutenberg.controller.commands.ReadFileCMD;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Johannes
 */
public class saveAs {

    private GDocument gDocument;
    private Invoker invoke;
    private GetFileTypeCMD fileType;

    /**
     * SaveAs constructor where a GDocument must be passed
     *
     * @param document
     */
    public saveAs(GDocument document) {
        this.gDocument = document;
        this.invoke = new Invoker();
    }

    /**
     * Opens a Save Dialog, to select a specific path where the text file should
     * be saved. A specific file name has to be entered.
     */
    public void SaveAs() {
        JFileChooser chooser = new JFileChooser();
        int result = chooser.showSaveDialog(gDocument.getjFrame()); // Parent must be a part of the
        // main window otherwise the dialog gets open on the default screen      
        File saveFile = chooser.getSelectedFile();
        int response = 0;

        if (result == JFileChooser.APPROVE_OPTION) {

            //<editor-fold defaultstate="collapsed" desc="      Check if file exists                   ">
            if (saveFile.exists()) {
                response = JOptionPane.showConfirmDialog(gDocument.getjFrame(), "Are you sure you want to overwrite the existing file?", "Confirm",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            }
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="  Check for overwrite and save file         ">

            if (response == 0) {
                try {
                    try (FileWriter fileWriter = new FileWriter(saveFile)) {
                        fileWriter.write(gDocument.getjTextPane().getText());
                        gDocument.setFileName(saveFile.getPath()); // gets path + filename e.g. /User/Gutenberg/Desktop/test.txt
                    }

                } catch (IOException e) {
                    JOptionPane.showMessageDialog(gDocument.getjFrame(), "IO Exception during SaveAs Dialog" + e.getMessage(), "Error Message", JOptionPane.WARNING_MESSAGE);
                }

                this.gDocument.setFile(saveFile);
                gDocument.getjFrame().setTitle(gDocument.getFileName());
            }
            //</editor-fold>

            if (gDocument.getjTextPane().getText().isEmpty()) {
                // Set syntax to JTextPane
                fileType = new GetFileTypeCMD(gDocument);
                fileType.execute();
                invoke.execute(new ChangeSyntaxCMD(gDocument, gDocument.getFileType()));
            } else if (!gDocument.getjTextPane().getText().isEmpty()) {
                // Set syntax to JTextPane
                fileType = new GetFileTypeCMD(gDocument);
                fileType.execute();
                invoke.execute(new ChangeSyntaxCMD(gDocument, gDocument.getFileType()));
                ReadFileCMD read = new ReadFileCMD(gDocument, saveFile);
                read.execute();
            }

        } else if (result == JFileChooser.CANCEL_OPTION) {
            gDocument.setFileName("Untitled");
            gDocument.getjFrame().setTitle(gDocument.getFileName());
        }
    }
}
