/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;

/**
 *
 * @author Johannes
 */
public class CompareStoredFileCMD implements ICommand {

    private GDocument gDocument;
    private boolean areEqual;
    private JTextPane bufferJTextPane;

    /**
     * Constructor for the ComparesSotredFileCMD
     *
     * @param gDocument
     */
    public CompareStoredFileCMD(GDocument gDocument) {
        this.gDocument = gDocument;
        this.bufferJTextPane = new JTextPane();
    }

    /**
     * Compares the actual gDocument with the stored file. If the gDocument has
     * changed, but was not stored the areEqaul variable will be set to false.
     */
    @Override
    public void execute() {
        if (gDocument.getFileName() != null) {
            File file = new File(gDocument.getFileName());

            if (file.exists() && file.canRead()) {

                try {
                    try (BufferedReader readTextFile = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
                        Object device = null;
                        bufferJTextPane.read(readTextFile, device);
                    }
                    bufferJTextPane.requestFocus();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ReadFileCMD.class.getName()).log(Level.SEVERE, null, ex);
                }

                areEqual = gDocument.getjTextPane().getText().equals(bufferJTextPane.getText());

            }
        }
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public boolean areEqual() {
        return areEqual;
    }
}
