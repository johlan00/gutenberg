# README #

![Alt text](http://seeksobriety.com/sites/default/files/gutenberg.jpg)
# Project Gutenberg #

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Short Description ##

Project: Gutenberg
Implementation of a text editor.

Please use the wiki documentation. The wiki can be accessed by pressing help in the Gutenberg editor.

Requirements:

### File ###
	* New
	* Open
	* Save
	* Save As
	* Exit

### Edit ####
	* Copy
	* Paste
	* Find
	* Find & Replace
 	* Undo-Function ---> Stichwort: 
    * Memento Design Pattern
    
### GUI-Controller ###

### GUI-View ###
	* Day & Night Mode
	* 1x Syntax-Highlighting (e.g, for Java)

### Assessment Criterion ###
	* functioning code
	* projekt description
	* documentation
	* UML pictures for the class description



