/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

/**
 *
 * @author Johannes
 */
public class DateAndTimeCMD implements ICommand {

    private GDocument gDocument;
    private AttributeSet as = null;
    private int caretPosition;

    /**
     * Constructor for the DateAndTimeCMD
     *
     * @param gDocument
     */
    public DateAndTimeCMD(GDocument gDocument) {
        this.gDocument = gDocument;
        this.caretPosition = 0;
    }

    /**
     * Inserts the local date and time using the following format. dd-MM-yyyy
     * HH-mm
     */
    @Override
    public void execute() {
        String dateAndTime;
        dateAndTime = new SimpleDateFormat("dd-MM-yyyy HH-mm").format(Calendar.getInstance().getTime());

        caretPosition = gDocument.getjTextPane().getCaretPosition();
        try {
            gDocument.getjTextPane().getDocument().insertString(caretPosition, dateAndTime, as);
        } catch (BadLocationException ex) {
            Logger.getLogger(DateAndTimeCMD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
