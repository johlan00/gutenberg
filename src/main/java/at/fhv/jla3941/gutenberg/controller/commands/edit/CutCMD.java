/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.fhv.jla3941.gutenberg.controller.commands.edit;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;

/**
 *
 * @author Johannes
 */
public class CutCMD implements ICommand {

    private GDocument gDocument;

    /**
     * Constructor for the CutCMD
     *
     * @param document
     */
    public CutCMD(GDocument document) {
        this.gDocument = document;
    }

    /**
     * Transfers the currently selected range in the associated text model to
     * the system clipboard, removing the contents from the model.
     */
    @Override
    public void execute() {
        gDocument.getjTextPane().cut();
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
