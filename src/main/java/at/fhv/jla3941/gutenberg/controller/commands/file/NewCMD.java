package at.fhv.jla3941.gutenberg.controller.commands.file;

import at.fhv.jla3941.gutenberg.controller.ICommand;
import at.fhv.jla3941.gutenberg.model.GDocument;
import at.fhv.jla3941.gutenberg.view.Gutenberg;
import java.awt.Point;


/**
 *
 * @author Johannes
 */
public class NewCMD implements ICommand {

    private GDocument gDocument;
    private Point location;

    public NewCMD(GDocument gDocument) {
        this.gDocument = gDocument;
    }
    
    /**
     * Creates a new Gutenberg Editor
     * The editor will not directly appear in front of the previous one,
     * there will be a small offset.
     */
    @Override
    public void execute() {
        Gutenberg newGutenberg = new Gutenberg();
        location = gDocument.getjFrame().getLocation();
        int offset = 20;
        newGutenberg.setLocation(location.x + offset , location.y + offset);
        newGutenberg.setVisible(true);
    }

    /**
     *
     */
    @Override
    public void unexecute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
